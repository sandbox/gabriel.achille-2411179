<?php

/**
 * @file media-youtube-extra-video.tpl.php
 *
 * Template file for theme('media_youtube_extra_video').
 *
 * Variables available:
 *  $video_id - The unique identifier of the YouTube video (e.g., xsy7x8c9).
 *  $image - video thumbnail
 *  $duration - video duration (formatted)
 *  $views_counter - nb of views
 *  $posted_date - video published date (formatted)
 *  $link_url - URL to link to
 *
 */

?>
<div class="<?php print $classes; ?> media-youtube-extra">
    <span class="video-img">
        <?php if($link_url): ?>
        <a href="<?php print $link_url; ?>">
            <?php print $image ?>
            <span class="video-time-duration"><?php print $duration; ?></span>
        </a>
        <?php else: ?>
            <?php print $image ?>
            <span class="video-time-duration"><?php print $duration; ?></span>
        <?php endif; ?>
    </span>
    <div class="video-description">
        <span class="video-views"><?php print $view_count; ?></span>
        <span class="video-time"><?php print $posted_date; ?></span>
    </div>
</div>
