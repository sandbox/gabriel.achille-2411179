INTRODUCTION
------------

This module extends the features of "Media: YouTube" by providing an access to the YouTube Data API (v3).


INSTALLATION
------------

  1. Download and enable the module.
  2. Configure at Administration > Configuration > Web services > Media: YouTube extra. Enter Google API key.


USAGE
------------

Configure the Video file type to use the new file formatter:
  1. Go to Administration > Structure > File Types > Video > Manage file display
  2. Untick "Youtube Preview Image" and "Youtube Video"
  3. Tick "Media YouTube advanced preview image" and choose a Image style below.
  4. Save

